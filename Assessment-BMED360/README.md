# Forms of Assessment BMED360 (10 ECTS)


## - Midterm project (15%):
**Compulsory assignment on given topic ([Kiwi segmentation challenge](https://github.com/computational-medicine/BMED360-2021/blob/main/Midterm-Kiwi-Project/README.md))**

## - MCQ / Quiz from home (10%):
**Compulsory (about 50 "simple/basic" MCQs / quizzes across the entire curriculum; 60 min duration - digital from home; at least 60\% correct to pass)**

## - Personal project / final oral exam on Zoom (75%):


1. Pick a topic from the BMED 360 curriculum that fits your research interests or evokes your curiosity (the topic could be e.g. your own research project, a scientific article, a review of methods with pros and cons).

2. Prepare a presentation of the topic as if you should give a talk [make the title yourself] at a scientific meeting with a very motivated audience - but who might not be experts in your field.   

3. For this purpose make a Google Slides (or PowerPoint, or Keynote, or Beamer) _presentation of 20 min duration_ and plan for a _5 min discussion_ with your audience  (i.e. sensor / examiner / student peers)

4. You might also write a manuscript or notes telling what to say during the presentation, but avoid reading directly from your manuscript during the talk as the notes are meant for rehearsal only.

5. The presentation produced in 3. must be sent/shared by email to the course coordinator at latest 2 hours before the exam.<br>  (You can ask for advice regarding topic selection or your planned talk in the early preparation phase)


6. At the oral exam (20 min talk using Zoom), the following criteria are **assessed**:
_scientific attitude_, _vocabulary_, and _balanced use of text, tables, and figures_; _clarity in presentation_ regarding problem formulation / introduction, methods description, results (if any), discussion, and conclusions; ability to bring enthusiasm for your topic to the audience; keeping time.<br>
Share your presentation with the examiner / sensor before the exam (see 5.).

7.   **Overall grading (spring 2021) is pass / fail**    (previously: grading scale was A-F)
