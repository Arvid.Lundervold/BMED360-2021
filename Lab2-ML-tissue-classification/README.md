# In Vivo Imaging and Physiological Modelling - BMED 360 

## Lab2-ML-tissue-classification

- [**00-recap-python.ipynb**](https://nbviewer.jupyter.org/github/computational-medicine/BMED360-2021/blob/main/Lab2-ML-tissue-classification/00-recap-python.ipynb) <a href="https://colab.research.google.com/github/computational-medicine/BMED360-2021/blob/main/Lab2-ML-tissue-classification/00-recap-python.ipynb">
  <img src="https://colab.research.google.com/assets/colab-badge.svg" alt="Open In Colab"/></a>


- [**01-intro-example.ipynb**](https://nbviewer.jupyter.org/github/computational-medicine/BMED360-2021/blob/main/Lab2-ML-tissue-classification/01-intro-example.ipynb) <a href="https://colab.research.google.com/github/computational-medicine/BMED360-2021/blob/main/Lab2-ML-tissue-classification/01-intro-example.ipynb">
  <img src="https://colab.research.google.com/assets/colab-badge.svg" alt="Open In Colab"/></a>



- [**02-extensive-example.ipynb**](https://nbviewer.jupyter.org/github/computational-medicine/BMED360-2021/blob/main/Lab2-ML-tissue-classification/02-extensive-example.ipynb) <a href="https://colab.research.google.com/github/computational-medicine/BMED360-2021/blob/main/Lab2-ML-tissue-classification/02-extensive-example.ipynb">
  <img src="https://colab.research.google.com/assets/colab-badge.svg" alt="Open In Colab"/></a>



- [**03-advanced-dl-example.ipynb**](https://nbviewer.jupyter.org/github/computational-medicine/BMED360-2021/blob/main/Lab2-ML-tissue-classification/03-advanced-dl-example.ipynb) <a href="https://colab.research.google.com/github/computational-medicine/BMED360-2021/blob/main/Lab2-ML-tissue-classification/03-advanced-dl-example.ipynb">
  <img src="https://colab.research.google.com/assets/colab-badge.svg" alt="Open In Colab"/></a>  Requires `fastai` kernel.



- [**04-mri-knn-tissue-classification.ipynb**](https://nbviewer.jupyter.org/github/computational-medicine/BMED360-2021/blob/main/Lab2-ML-tissue-classification/04-mri-knn-tissue-classification.ipynb) <a href="https://colab.research.google.com/github/computational-medicine/BMED360-2021/blob/main/Lab2-ML-tissue-classification/04-mri-knn-tissue-classification.ipynb"> <img src="https://colab.research.google.com/assets/colab-badge.svg" alt="Open In Colab"/></a>



- [**05-mri-kmeans-tissue-classification.ipynb**](https://nbviewer.jupyter.org/github/computational-medicine/BMED360-2021/blob/main/Lab2-ML-tissue-classification/05-mri-kmeans-tissue-classification.ipynb) <a href="https://colab.research.google.com/github/computational-medicine/BMED360-2021/blob/main/Lab2-ML-tissue-classification/05-mri-kmeans-tissue-classification.ipynb"> <img src="https://colab.research.google.com/assets/colab-badge.svg" alt="Open In Colab"/></a>



- [**06-mri-freesurfer-segmentation-bash.ipynb**](https://nbviewer.jupyter.org/github/computational-medicine/BMED360-2021/blob/main/Lab2-ML-tissue-classification/06-mri-freesurfer-segmentation-bash.ipynb). Requires `Freesurfer`.



## Additional sources for ML

- **Preprocessing data** (_standardization_ [zero mean and unit variance], _normalization_, _discretization_, and more) - https://scikit-learn.org/stable/modules/preprocessing.html
- **Nearest Neighbors** (unsupervised and supervised _NN classification_ and _NN regression_) - https://scikit-learn.org/stable/modules/neighbors.html
- **Cross-validation**: evaluating estimator performance - https://scikit-learn.org/stable/modules/cross_validation.html
- **Ensemble methods** (_weak learners_, _voting classifier_, _stacking_, and more) - https://scikit-learn.org/stable/modules/ensemble.html
- **Clustering** (_K-means_, _agglomerative_, clustering performance evaluation, and more) - https://scikit-learn.org/stable/modules/clustering.html
- **Metrics and scoring**: quantifying the quality of predictions - https://scikit-learn.org/stable/modules/model_evaluation.html
