# In Vivo Imaging and Physiological Modelling - BMED 360


### Before you start on this Lab1-MRI (also introducing IMC): 


#### Activate the environment:
```bash
conda activate bmed360
```

-------------------------

# Learning objectives

In the following series of four Jupyter notebooks will explore **multichannel imaging data** from two quite different **imaging modalities** (in terms of physical principles and spatial resolution): 
- **Imaging Mass Cytometry** ([IMC](./IMC.md))
- **Magnetic Resonance Imaging** (MRI: see below)

We will also illustrate the **generic nature of computational imaging**, here: unsupervised tissue classification, applied to these two kinds of vector-valued image data recordings.

----------


- [**00-get-mri-imc-data.ipynb**](https://nbviewer.jupyter.org/github/computational-medicine/BMED360-2021/blob/main/Lab1-MRI/00-get-mri-imc-data.ipynb) <a href="https://colab.research.google.com/github/computational-medicine/BMED360-2021/blob/main/Lab1-MRI/00-get-mri-imc-data.ipynb">
  <img src="https://colab.research.google.com/assets/colab-badge.svg" alt="Open In Colab"/></a>

Download the IMC and MRI data from Goggle Drive cloud:

```
Lab1-MRI% tree data

data
├── imc
│   ├── E08_a0_full.csv
│   ├── E08_a0_full.tiff
│   └── table1_IMC_panel_37x4.csv
└── mri
    ├── 0.0-test_nifti.nii.gz
    ├── BraTS20
    │   ├── BraTS20_Training_002_HDGlioSeg.nii.gz
    │   ├── BraTS20_Training_002_flair.nii.gz
    │   ├── BraTS20_Training_002_seg.nii.gz
    │   ├── BraTS20_Training_002_t1.nii.gz
    │   ├── BraTS20_Training_002_t1ce.nii.gz
    │   └── BraTS20_Training_002_t2.nii.gz
    ├── brain_roi_mask.nii.gz
    ├── dess_060.dcm
    ├── dess_060.nii.gz
    ├── fisp_060.dcm
    ├── fisp_060.nii.gz
    ├── flash_060.dcm
    ├── flash_060.nii.gz
    ├── flash_060_brain_mask.png
    ├── flash_060_training_mask_6cla.png
    ├── mni_icbm152_t1_tal_nlin_sym_09c.nii.gz
    ├── multispectral_mri_training_data.csv
    ├── psif_060.dcm
    ├── psif_060.nii.gz
    └── training_mask_1_6.nii.gz
```
<!--
```
% tree data
data
├── imc
│   ├── E08_a0_full.csv
│   ├── E08_a0_full.tiff
│   ├── E08_a0_panel_data_and_channel_numbering.csv
│   └── table1_IMC_panel_37x4.csv
└── mri
    ├── 0.0-test_nifti.nii.gz
    ├── brain_roi_mask.nii.gz
    ├── dess_060.dcm
    ├── dess_060.nii.gz
    ├── fisp_060.dcm
    ├── fisp_060.nii.gz
    ├── flash_060.dcm
    ├── flash_060.nii.gz
    ├── flash_060_brain_mask.png
    ├── flash_060_training_mask_6cla.png
    ├── mni_icbm152_t1_tal_nlin_sym_09c.nii.gz
    ├── multispectral_mri.nii.gz
    ├── multispectral_mri_training_data.csv
    ├── multispectral_mri_training_data_from_nifti_mask.csv
    ├── psif_060.dcm
    ├── psif_060.nii.gz
    └── training_mask_1_6.nii.gz
```
-->

- [**01-mri-intro.ipynb**](https://nbviewer.jupyter.org/github/computational-medicine/BMED360-2021/blob/main/Lab1-MRI/01-mri-intro.ipynb) <a href="https://colab.research.google.com/github/computational-medicine/BMED360-2021/blob/main/Lab1-MRI/01-mri-intro.ipynb">
  <img src="https://colab.research.google.com/assets/colab-badge.svg" alt="Open In Colab"/></a>


- [**02-mri-multispectral.ipynb**](https://nbviewer.jupyter.org/github/computational-medicine/BMED360-2021/blob/main/Lab1-MRI/02-mri-multispectral.ipynb) <a href="https://colab.research.google.com/github/computational-medicine/BMED360-2021/blob/main/Lab1-MRI/02-mri-multispectral.ipynb">
  <img src="https://colab.research.google.com/assets/colab-badge.svg" alt="Open In Colab"/></a>


- [**03-mri-snr-cnr.ipynb**](https://nbviewer.jupyter.org/github/computational-medicine/BMED360-2021/blob/main/Lab1-MRI/03-mri-snr-cnr.ipynb) <a href="https://colab.research.google.com/github/computational-medicine/BMED360-2021/blob/main/Lab1-MRI/03-mri-snr-cnr.ipynb">
  <img src="https://colab.research.google.com/assets/colab-badge.svg" alt="Open In Colab"/></a>

- [**04-imc-Introductory.ipynb**](https://nbviewer.jupyter.org/github/computational-medicine/BMED360-2021/blob/main/Lab1-MRI/04-imc-intro.ipynb) <a href="https://colab.research.google.com/github/computational-medicine/BMED360-2021/blob/main/Lab1-MRI/04-imc-intro.ipynb">
    <img src="https://colab.research.google.com/assets/colab-badge.svg" alt="Open In Colab"/></a>

----------------

## Sources of information related to MRI principles and applications

### Introductory videos
- **How Does an MRI Scan Work?** 1:20 (https://youtu.be/1CGzk-nV06g)
- **MRI vs. CT** 2:31 (https://youtu.be/aQZ8tTZnQ8A)
- **Brain MRI scan protocols, positioning and planning** 9:34 (https://youtu.be/R5LQzoFynqI)
- **MRI: Basic Physics & a Brief History**  25:51 (https://youtu.be/djAxjtN_7VE)
- **Bergen fMRI group - the birth of a research group** 19:45 (https://youtu.be/6UhfAX3RusE)

### Awesome Magnetic Resonance Imaging (MRI)
 - **A curated list of delightful Magnetic Resonance courses, books, lectures, papers, blogs and free resources** by Daniel Gomez, Donders Institute for Brain, Cognition and Behaviour (https://github.com/dangom/awesome-mri)
 - **MRIMASTER.COM** (https://mrimaster.com)

### Simulators and more

- **The (web) compass simulator** from the Danish Research Centre for Magnetic Resonance and TDU  (http://www.drcmr.dk/CompassMR)
- **The (web) [Bloch simulator](http://drcmr.dk/new-bloch-simulator)** from the Danish Research Centre for Magnetic Resonance and TDU (http://drcmr.dk/BlochSimulator)
- **MRiLab** A Numerical Magnetic Resonance Imaging Simulation Platform in MATLAB by [Fang Liu](http://fliu37.com)(https://github.com/leoliuf/MRiLab)

- **MRI-education-resources** UCSF Peder Larson Research Group
 (https://github.com/LarsonLab/MRI-education-resources/tree/master/Notebooks)

- **Pulse sequence graphics** by Daniel Gomez (https://github.com/dangom/mr-sequence-diagrams/blob/master/README.org)


----------------------

#### To relax and enjoy biological vision (in spare time) - see
[this webpage](https://michaelbach.de/ot) containing demos of many beautiful and fascinating optical illusions and visual phenomena. Professor Michael Bach gives detailed descriptions of these phenomena also from a theoretical perspective.
Even more interested? see https://foundationsofvision.stanford.edu by Prof. Brian A. Wandell at Stanford.
