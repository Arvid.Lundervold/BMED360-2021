# Deep learning - extracurricular activity 

 _Educational activities not falling within the scope of the regular BMED360 curriculum_

### DAT255-1 21V Practical deep learning (10 ECTS @HVL)
- Course description - https://www.hvl.no/en/studies-at-hvl/study-programmes/courses/34/dat255
- Canvas (hvl.instructure.com) at HVL - https://hvl.instructure.com/courses/14041
- [About the course](https://hvl.instructure.com/courses/14041/modules#module_51744)
- Textbook: J. Howard and S. Gugger, [_Deep Learning for Coders with fastai and PyTorch_](https://www.oreilly.com/library/view/deep-learning-for/9781492045519). O'Reilly 2020.
   - The complete book is freely available as Jupyter Notebooks in the book repo: https://github.com/fastai/fastbook
- GitHub repo for the **fastai** course - https://course.fast.ai

### Sebastian Raschka (STAT 453 @ UW-Madison / PyTorch)

- Book Review: Deep Learning With PyTorch  ([PDF](https://pytorch.org/assets/deep-learning/Deep-Learning-with-PyTorch.pdf)) - https://sebastianraschka.com/blog/2021/pytorch-deeplearning-review.html
- STAT 453 -- Introduction to Deep Learning and Generative Models (Spring 2021) - http://pages.stat.wisc.edu/~sraschka/teaching/stat453-ss2021
- STAT 453: Intro to Deep Learning @ UW-Madison (Spring 2021) - https://github.com/rasbt/stat453-deep-learning-ss21



### Tensorly (tensor learning i python)

- Jean Kossaifi, Yannis Panagakis, Anima Anandkumar and Maja Pantic, TensorLy: Tensor Learning in Python, _Journal of Machine Learning Research_ (JMLR) 2019;20(26):1-6.   http://jmlr.org/papers/v20/18-277.html  / https://github.com/tensorly/tensorly
- [**tensor_manipulation.ipynb**](https://nbviewer.jupyter.org/github/computational-medicine/BMED360-2021/blob/main/Deep-Learning-Extra/01-basics/tensor_manipulation.ipynb)
